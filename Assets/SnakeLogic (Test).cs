using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeLogic : MonoBehaviour
{
    private float timer = 0;
    private int movementX = 0;
    private int movementY = 0;
    public GameObject snake;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer > 0.5f)
        {
           
            snake.transform.position += new Vector3(movementX, movementY);
            timer = 0;
            //FindObjectOfType<GameLogic>().checkForFood(Mathf.FloorToInt(snake.transform.position.x), Mathf.FloorToInt(snake.transform.position.y), snake.transform);
           
        }

        if (Input.GetKey("down")){
            movementY = -1;
            movementX = 0;
            
        }
        if (Input.GetKey("up"))
        {
            movementY = 1;
            movementX = 0;
            
        }
        if (Input.GetKey("left"))
        {
            movementY = 0;
            movementX = -1;
          
        }
        if (Input.GetKey("right"))
        {
            movementY = 0;
            movementX = 1;
           
        }
        
    }
}
