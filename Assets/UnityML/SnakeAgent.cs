using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class SnakeAgent : Agent
{
    // Start is called before the first frame update
    Rigidbody2D rBody;
    GameLogic game;
    FoodLogic Target;
    void Start()
    {
      rBody = GetComponent<Rigidbody2D>();
        game = FindObjectOfType<GameLogic>();
       
    }

    
    public override void OnEpisodeBegin()
    {
        
    }
    public override void CollectObservations(VectorSensor sensor)
    {
        Target = FindObjectOfType<FoodLogic>();
        // Target and Agent positions
        
        if (Target != null)
        {
            sensor.AddObservation(Target.transform.localPosition);
        } 
        sensor.AddObservation(this.transform.localPosition);

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.y);
    }

    public float forceMultiplier = 10;
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        // Actions, size = 2
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = actionBuffers.ContinuousActions[0];
        controlSignal.y = actionBuffers.ContinuousActions[1];
        rBody.AddForce(controlSignal * forceMultiplier);
        float distanceToTarget = 0;
        // Rewards
        if (Target != null)
        {
            distanceToTarget = Vector3.Distance(this.transform.localPosition, Target.transform.localPosition);
        }
        
        // Reached target
        if(game.getScore() > 10)
        {
            EndEpisode();
            game.score = 0;
        }

        if(gameObject.transform.position.y > GameLogic.HEIGHT-GameLogic.HEIGHT/2 - 0.5 || gameObject.transform.position.y < 0.5 - GameLogic.HEIGHT / 2
            || gameObject.transform.position.x > GameLogic.WIDTH-GameLogic.WIDTH/2 - 0.5 || gameObject.transform.position.x < 0.5 - GameLogic.WIDTH / 2)
        {
            AddReward(-0.5f);
        }

        
        if (game.timeFoodEaten > 30f)
        {
            if(distanceToTarget < 0.2f)
            {
                print(1);
                AddReward(1);
            }else
            {
                print(distanceToTarget * -0.9f);
                AddReward(distanceToTarget * -0.9f);
            }
           
        }
        //when the snake hasnt eaten in 60 seconds
        if (game.timeFoodEaten > 60f)
        {            
            game.timeFoodEaten = 0f;
            EndEpisode();
            game.score = 0;
            gameObject.transform.position = Vector3.zero;
        }

        
       

       
    }

    
    public void notifyScore(int score)
    {
        print(15);
        AddReward(15.0f);
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continuousActionsOut = actionsOut.ContinuousActions;
        continuousActionsOut[0] = Input.GetAxis("Horizontal");
        continuousActionsOut[1] = Input.GetAxis("Vertical");
    }
}
