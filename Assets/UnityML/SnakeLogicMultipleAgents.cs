using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class SnakeLogicMultipleAgents : Agent
{
    // Start is called before the first frame update
    Rigidbody2D rBody;
    GameLogic game;
    FoodLogic Target;
    SnakeLogicMultipleAgents[] agents;
    bool freeze = false;
    public GameObject myLaser;
    [SerializeField]
    public float COOLDOWNTIME = 5;
    [SerializeField]
    public float FREEZETIME = 2;
    [SerializeField]
    private float timerCooldown;
    [SerializeField]
    private float timerFreeze;
    [SerializeField]
    public float timerFoodEaten;
    
    void Start()
    {
        rBody = GetComponent<Rigidbody2D>();
        game = FindObjectOfType<GameLogic>();

    }


    public override void OnEpisodeBegin()
    {

    }
    public override void CollectObservations(VectorSensor sensor)
    {
        Target = FindObjectOfType<FoodLogic>();
        agents = FindObjectsOfType<SnakeLogicMultipleAgents>();
        // Target and Agent positions

        if (Target != null)
        {
            sensor.AddObservation(Target.transform.localPosition);
        }else
        {
            sensor.AddObservation(-1);
        }

        if (agents[0] != null)
        {
            sensor.AddObservation(agents[0].transform.localPosition);
        }
        else
        {
            sensor.AddObservation(-1);
        }

        sensor.AddObservation(this.transform.localPosition);

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.y);
    }

    public float forceMultiplier = 10;
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        // Actions, size = 2
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = actionBuffers.ContinuousActions[0];
        controlSignal.y = actionBuffers.ContinuousActions[1];
        if (!freeze)
        {
            rBody.AddForce(controlSignal * forceMultiplier);
        }
        else if (timerFreeze < game.timer) {

            freeze = false;
           
        }
        float distanceToTarget = 0;
        // Rewards
        if (Target != null)
        {
            distanceToTarget = Vector3.Distance(this.transform.localPosition, Target.transform.localPosition);
        }

        if (actionBuffers.DiscreteActions[0] > 0)
        {
            shootLaser(actionBuffers.DiscreteActions[0]);
        }
        else
        {
            
            
            myLaser.transform.localScale = new Vector3(0f, 0f, 0f);
          
        }

        // Reached target
        if (game.getScore() > 5)
        {
            EndEpisode();
            game.score = 0;
        }

        //make this local position OR remove it entirely
        //if (gameObject.transform.position.y > GameLogic.HEIGHT - GameLogic.HEIGHT / 2 - 0.5 || gameObject.transform.position.y < 0.5 - GameLogic.HEIGHT / 2
        //    || gameObject.transform.position.x > GameLogic.WIDTH - GameLogic.WIDTH / 2 - 0.5 || gameObject.transform.position.x < 0.5 - GameLogic.WIDTH / 2)
        //{
        //    AddReward(-0.5f);
        //}

        //this should be changed since it simply dances around the food to get the points
        //I changed this multiple times
        if (timerFoodEaten > 15f)
        {
            if (distanceToTarget < 0.1f)
            {
                print("rewarded distance");
                AddReward(0.01f);
            }
            else
            {
                print("penalty distance");
                AddReward(distanceToTarget * -0.9f);
            }

        }
        //when the snake hasnt eaten in 30 seconds
        if (timerFoodEaten > 30f)
        {
            timerFoodEaten = 0f;
            EndEpisode();
            game.score = 0;
            gameObject.transform.position = Vector3.zero;
        }





    }
    
    public void shootLaser(int direction)
    {
       
        if (timerCooldown < game.timer)
        {

        //1 is up
        //2 right
        //3 is down
        //4 is left
        Vector3 laserDirection = Vector3.zero;
        Vector3 laserRotation = Vector3.zero;
        Vector3 startPosition = Vector3.zero;
        switch (direction)
        {
            case 1:
                laserDirection = new Vector3(0f, 3f);
                laserRotation.z = 90;
                startPosition = new Vector3(0f, 0.6f);
                break;

            case 2:
                laserDirection = new Vector3(3f, 0f);
                laserRotation.z = 0;
                startPosition = new Vector3(0.6f, 0f);
                break;
               
            case 3:
                laserDirection = new Vector3(0f, -3f);
                laserRotation.z = 90;
                startPosition = new Vector3(0f, -0.6f);
                break;

            case 4:
                laserDirection = new Vector3(-3f ,0f);
                laserRotation.z = 0;
                startPosition = new Vector3(-0.6f, 0f);
                break;
        }


        var myTransform = transform;
        myLaser.transform.localPosition = laserDirection;
        myLaser.transform.localEulerAngles = laserRotation;
        myLaser.transform.localScale = new Vector3(1f, 0.1f);
        var rayDir = laserDirection * 1f;
        Debug.DrawRay(myTransform.position + startPosition, rayDir, Color.blue, 0f, true);
        RaycastHit2D hit = Physics2D.Raycast(myTransform.position + startPosition, new Vector2(rayDir.x, rayDir.y), 5f);
            Debug.DrawLine(transform.position + startPosition, hit.point, Color.green);

            if (hit)
            {
                print("I shot something!");
                 
                if (hit.collider.gameObject.CompareTag("agent"))
                {
                    hit.collider.gameObject.GetComponent<SnakeLogicMultipleAgents>().Freeze();
                    AddReward(1f);
                    print("rewared laser");
                }
                timerCooldown = game.timer + COOLDOWNTIME;
            }
            else
            {
                AddReward(-0.1f);
                print("penalty laser");
                timerCooldown = game.timer + COOLDOWNTIME;
            }
        }
    }

    public void Freeze()
    {
        freeze = true;
        timerFreeze = game.timer + FREEZETIME;
    }

    public void notifyScore(int score)
    {
        print("rewarded food");
        //maybe this should be more
        AddReward(30.0f);
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continuousActionsOut = actionsOut.ContinuousActions;
        var discreteAction = actionsOut.DiscreteActions;
        continuousActionsOut[0] = Input.GetAxis("Horizontal");
        continuousActionsOut[1] = Input.GetAxis("Vertical");
        if (Input.GetKey(KeyCode.UpArrow))
        {
            discreteAction[0] = 1;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            discreteAction[0] = 3;
        }
        if (Input.GetKey(KeyCode.LeftArrow)){
            discreteAction[0] = 2;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            discreteAction[0] = 4;
        }
    }
}
