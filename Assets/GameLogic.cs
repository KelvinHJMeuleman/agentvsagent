using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GameLogic : MonoBehaviour
{


    public float timeFoodEaten = 0;
    public float timer = 0;
    public GameObject food;
    public static float WIDTH = 0;
    public static float HEIGHT = 0;
    public int score = 0;
    Transform[,] grid = new Transform[Mathf.FloorToInt(WIDTH), Mathf.FloorToInt(HEIGHT)];

    /**
    internal void removeFromGrid(int x, int y)
    {
        x += Mathf.FloorToInt(WIDTH / 2);
        y += Mathf.FloorToInt(HEIGHT / 2);
        grid[x, y] = null;
    }

    internal void checkForFood(int x, int y, Transform transform)
    {
        x += Mathf.FloorToInt(WIDTH / 2);
        y += Mathf.FloorToInt(HEIGHT / 2);
        FoodLogic food = FindObjectOfType<FoodLogic>();
        if (grid[x,y] != null)
        {
            food.foodEaten();
        }
    }

    internal void addToGrid(int x, int y, Transform transform)
        
    {
   
        x += Mathf.FloorToInt(WIDTH / 2);
        y += Mathf.FloorToInt(HEIGHT / 2); 
        if(grid[x,y] == null)
        {
            grid[x, y] = transform;
        }
        
    }
    **/
    // Start is called before the first frame update
    void Start()
    {
        HEIGHT = gameObject.transform.localScale.y - 1;
        WIDTH = gameObject.transform.localScale.x -1 ;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        foreach(SnakeLogicMultipleAgents snake in FindObjectsOfType<SnakeLogicMultipleAgents>())
        {
            snake.timerFoodEaten += Time.deltaTime;
        }
       
            if (GameObject.FindGameObjectsWithTag("Food").Length < 4)
            {
                Vector3 randomPos = new Vector3((int)(Random.value * WIDTH) - WIDTH / 2, (int)(Random.value * HEIGHT) - HEIGHT/ 2);

                //addToGrid(Mathf.FloorToInt(randomPos.x) , Mathf.FloorToInt(randomPos.y),Instantiate(food, randomPos, new Quaternion()).transform);
                Instantiate(food, randomPos, new Quaternion(),this.transform);                
            }
        
      
    }

    

    public void addScore(int score, SnakeLogicMultipleAgents agent)
    {
        this.score += score;
        agent.notifyScore(this.score);
        agent.timerFoodEaten = 0;
    }

    public int getScore()
    {
        return score;
    }

 
}
